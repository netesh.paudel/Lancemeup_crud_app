import 'dart:convert';

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:lancemeupmovie/data/app_execption.dart';
import 'package:lancemeupmovie/data/network/base_api_services.dart';

import 'package:lancemeupmovie/data/network/constant/base_url.dart';
import 'package:lancemeupmovie/data/network/constant/headers.dart';
import 'package:lancemeupmovie/models/post_model.dart';
import 'package:lancemeupmovie/utlis/snackbar.dart';
import 'package:lancemeupmovie/view/homepage/view_post/widgets/Inputfield.dart';
import 'package:lancemeupmovie/view_model/editedText.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NetworkAPIService extends BaseAPIService with ChangeNotifier {
  @override
  Future<List<PostModel>> getAPIResponse(context) async {
    try {
      final response = await http.get(Uri.parse(baseUrl + endPoints));
      var result = json.decode(response.body);
      if (response.statusCode == 200) {
        var data = postModelFromJson(response.body);

        return data;
      } else {
        return result;
      }
    } on SocketException {
      return Utlis().showSnakbar("No internet connection ", context);
    }
  }

  @override
  postAPIResponse(context, args) async {
    try {
      final response = await http.post(Uri.parse("$baseUrl$endPoints"),
          body: jsonEncode(<String, dynamic>{
            "userId": args["userId"],
            "id": args["id"],
            "title": args["title"],
            "body": Provider.of<InputField>(context, listen: false).editedPost
          }),
          headers: headers);
      var data = response.body;

      editedText = await json.decode(data);
      SharedPreferences sp = await SharedPreferences.getInstance();
      sp.setString(editedText["title"], editedText["body"]);
      notifyListeners();
    } on SocketException {
      throw FetchDataException("No Internet Connection");
    }
  }

  @override
  Future deleteAPIResponse(context, args) async {
    try {
      var result = false;

      var response = await http
          .delete(Uri.parse("$baseUrl$endPoints/${args["index"] + 1}"),
              headers: headers)
          .then((value) {
        return result = true;
      });

      return result;
    } on SocketException {
      throw FetchDataException("No Internet Connection");
    }
  }
}
