import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:lancemeupmovie/respository/shared_prefrences.dart';
import 'package:lancemeupmovie/utlis/snackbar.dart';
import 'package:lancemeupmovie/view/authscreen/signupscreen/widgets/input_fields.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constant/base_url.dart';
import 'package:http/http.dart' as http;

class AuthAPIServices with ChangeNotifier {
  var data;
  Future authAPIResponse(context) async {
    String email = Provider.of<Textformfiled>(context, listen: false).email;
    String password =
        Provider.of<Textformfiled>(context, listen: false).password;
    debugPrint(email);
    try {
      final response = await http.post(
        Uri.parse("$authBaseUrl$signEndPoints"),
        body: {
          "email": email,
          "password": password,
        },
      );
      if (response.statusCode == 200) {
        saveUser(context);
        return Utlis().showSnakbar("Account Registered successfully", context);
      } else {
        return Utlis().showSnakbar("Something went wrong", context);
      }
    } on SocketException {
      throw Utlis().showSnakbar("something went wrong", context);
    }
  }
}
