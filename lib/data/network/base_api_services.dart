abstract class BaseAPIService {
  Future<dynamic> getAPIResponse(context);
  Future<dynamic> postAPIResponse(context, args);
  Future<dynamic> deleteAPIResponse(context, args);
}
