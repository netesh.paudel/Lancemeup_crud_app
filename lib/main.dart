import 'package:flutter/material.dart';
import 'package:lancemeupmovie/view/myapp.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // ignore: unused_local_variable
  final sharedPreferences = await SharedPreferences.getInstance();
  runApp(const MyApp());
}
