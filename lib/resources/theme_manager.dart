import 'package:flutter/material.dart';

import 'color_manager.dart';
import 'font_manager.dart';
import 'style_manager.dart';

ThemeData applicationThemeData() {
  return ThemeData(
      //splashColor: Colors.transparent,
      scaffoldBackgroundColor: ColorManager.bgColor,
      appBarTheme: AppBarTheme(
          color: ColorManager.greenThemeColor,
          centerTitle: true,
          iconTheme: IconThemeData(color: ColorManager.darkBgPrimary),
          elevation: 0,
          titleTextStyle: getRegularTextStyle(
              color: ColorManager.lightFontcolor,
              fontSize: FontSizeManager.f18,
              fontWeight: FontWeightManager.semiBold)),
      iconTheme: IconThemeData(color: ColorManager.blackIconColor),
      inputDecorationTheme: InputDecorationTheme(
        iconColor: ColorManager.greyIconColor,
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(color: ColorManager.error),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: ColorManager.error),
          borderRadius: BorderRadius.circular(5),
        ),
        focusColor: Colors.white,
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(color: ColorManager.redcolor),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: ColorManager.greycolor),
          borderRadius: BorderRadius.circular(5),
        ),
      ),
      textButtonTheme: TextButtonThemeData(
          style: ButtonStyle(
              overlayColor: MaterialStateProperty.all(Colors.transparent))));
}
