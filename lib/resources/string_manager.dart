class StringManager {
  static String appName = "CRUD App";
  static String viewPost = "View Post";
  static String id = "Id : ";
  static String title = "Title : ";
  static String body = "Body :　";
  static String post = "Post No : ";
  static String username = "Dummy Name";
  static String email = "dummy@gmail.com";
  static String editPost = "Edit Post";
  static String updatePost = "Update Post";
  static String deletePost = "Delete Post";
  static String areyoursure = "Are you sure want to delete post ?";
  static String postupdated = "Post updated successfully";
  static String postdeleted = "Post deleted successfully";
  static String Email = "Email";
  static String password = "Password";
  static String signup = "Signup";
  static String hintEmailText = "Enter your Email";
  static String emailValidate = "Enter valid Email";
  static String hintPasswordText = "Enter your Password";
  static String passwwordValidate = "Enter valid Password";
}
