import 'package:flutter/material.dart';

class ColorManager {
  //screencolors
  static Color primaryBgColor = Colors.white;
  static Color greenThemeColor = const Color(0xff63B246);
  static Color greenbg = const Color(0xff22C55E);
  static Color darkBgPrimary = const Color(0xFF100E12);
  static Color greycolor = const Color(0xff8896AB);
  static Color darkGreenColor = const Color(0xff16A34A);
  static Color lightGreycolor = const Color(0xffD5DAE1);
  static Color lightGreycolor2 = const Color(0xffEEF0F3);
  static Color circleavatarbgColor = const Color(0xffBBF7D0);
  static Color redcolor = const Color(0xffFC4141);

  //fontcolors
  static Color lightFontcolor = Colors.white;
  static Color greyFontcolor = const Color(0xff8896AB);
  static Color greenFontcolor = const Color(0xff63B246);
  static Color darkFontcolor = Colors.black;

  //button colors
  static Color redButtoncolor = const Color(0xffFC4141);
  static Color greenButtoncolor = const Color(0xff62B246);
  //error color
  static Color error = const Color(0xFFe61f34);

  //icon color
  static Color blackIconColor = Colors.black;
  static Color greenIconColor = const Color(0xff63B246);
  static Color greyIconColor = const Color(0xff8896AB);
  static Color yellowIconColor = const Color(0xffEE961B);
  static Color blueIconColor = const Color(0xff1C6DF0);
  static Color lightGreenIconColor = const Color(0xffBBF7D0);
  static Color lightBGWhiteColor = const Color.fromARGB(255, 245, 245, 246);

  static Color selectedColor = const Color(0xffDD3B46);
  static Color bgColor = const Color(0xffF0F0F0);
  static Color fontColorBlack = Colors.black;
  static Color fontColorwhite = Colors.white;
  static Color labelTextColorBlack = const Color(0xff94949E);
  static Color fontColorRed = const Color(0xffDD3B46);
  static Color iconColorBlack = Colors.black;
  static Color iconColorWhite = Colors.white;
  static Color iconColorRed = const Color(0xffDD3B46);
  static Color cardColor = Colors.white;
  static Color shadowColorBlack = Colors.black;
  static Color btnColorRed = const Color(0xffDD3B46);
  static Color btnColorBlack = Colors.black;
  static Color btnColorwhite = Colors.white;
  static Color drawerColorRed = const Color(0xffDD3B46);
  static Color indicatorColor = const Color(0xffDD3B46);
  static Color snackBarBGColor = const Color(0xffDD3B46);
  static Color dividerColorBlack = Colors.black;
  static Color dividerColorRed = const Color(0xffDD3B46);
  static Color dividerColorWhite = Colors.white;
}
