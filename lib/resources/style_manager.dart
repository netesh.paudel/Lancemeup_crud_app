import 'package:flutter/material.dart';

TextStyle _textStyle(double fontSize, Color color, fontWeight, fontfamily) {
  return TextStyle(
      fontSize: fontSize,
      color: color,
      fontWeight: fontWeight,
      fontFamily: "Urbanist");
}

TextStyle getRegularTextStyle(
    {required fontSize, required Color color, fontWeight, fontfamily}) {
  return _textStyle(fontSize, color, fontWeight, fontfamily);
}
