class MarginManager {
  static const double m8 = 8.0;
  static const double m10 = 10.0;
  static const double m12 = 12.0;
  static const double m14 = 14.0;
  static const double m16 = 16.0;
  static const double m18 = 18.0;
  static const double m20 = 20.0;
}

class HeightManger {
  static const double h4 = 4.0;
  static const double h6 = 6.0;
  static const double h8 = 8.0;
  static const double h10 = 10.0;
  static const double h12 = 12.0;
  static const double h14 = 14.0;
  static const double h16 = 16.0;
  static const double h18 = 18.0;
  static const double h20 = 20.0;
  static const double h24 = 24.0;
  static const double h25 = 25.0;
  static const double h30 = 30.0;
  static const double h32 = 32.0;
  static const double h36 = 36.0;
  static const double h40 = 40.0;
  static const double h48 = 48.0;
  static const double h56 = 56.0;
  static const double h60 = 60.0;
  static const double h72 = 72.0;
  static const double h100 = 100.0;

  static const double h150 = 150.0;
  static const double h160 = 160.0;
  static const double h220 = 220;
}

class WidthManger {
  static const double w4 = 4.0;
  static const double w6 = 6.0;
  static const double w8 = 8.0;
  static const double w10 = 10.0;
  static const double w12 = 12.0;
  static const double w14 = 14.0;
  static const double w16 = 16.0;
  static const double w18 = 18.0;
  static const double w20 = 20.0;
  static const double w24 = 24.0;
  static const double w25 = 25.0;
  static const double w30 = 30.0;
  static const double w32 = 32.0;
  static const double w34 = 34.0;
  static const double w36 = 36.0;
  static const double w48 = 48.0;
  static const double w52 = 52.0;
  static const double w131 = 131.0;

  static const double w178 = 178.0;
  static const double w200 = 200.0;
  static const double w358 = 358.0;
}

class PaddingManager {
  static const double p4 = 4.0;
  static const double p8 = 8.0;
  static const double p10 = 10.0;
  static const double p12 = 12.0;
  static const double p14 = 14.0;
  static const double p16 = 16.0;
  static const double p18 = 18.0;
  static const double p20 = 20.0;
  static const double p26 = 26.0;
  static const double p32 = 32.0;
  static const double p40 = 40.0;
  static const double p60 = 60.0;
}

class AppSize {
  static const double ap1_5 = 1.5;
  static const double ap4 = 4.0;
  static const double ap8 = 8.0;
  static const double as10 = 10.0;
  static const double as12 = 12.0;
  static const double as14 = 14.0;
  static const double as16 = 16.0;
  static const double as18 = 18.0;
  static const double as20 = 20.0;
  static const double as40 = 40.0;
  static const double as60 = 60.0;
}

class BorderRadiusManager {
  static const double br4 = 4.0;
  static const double br6 = 6.0;
  static const double br8 = 8.0;
  static const double br10 = 10.0;
  static const double br12 = 12.0;
  static const double br14 = 14.0;
  static const double br16 = 16.0;
  static const double br18 = 18.0;
  static const double br20 = 20.0;
  static const double br24 = 24.0;
  static const double br26 = 26.0;
  static const double br32 = 32.0;
  static const double br36 = 36.0;
  static const double br48 = 48.0;
}

class Durationconst {
  static const int d300 = 300;
}
