import '../models/post_model.dart';

class ScreenArguments {
  int index;

  List<PostModel> postmodel;

  ScreenArguments({required this.index, required this.postmodel});
}
