import 'package:flutter/material.dart';
import 'package:lancemeupmovie/view_model/editedText.dart';

class CheckIndex with ChangeNotifier {
  checkIndex(args) {
    if (args["title"] == editedText["title"]) {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        notifyListeners();
      });
      return editedText["body"];
    } else {
      return args["body"];
    }
  }
}
