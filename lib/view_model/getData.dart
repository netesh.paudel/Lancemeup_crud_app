import 'package:flutter/material.dart';

import '../data/network/network_api_service.dart';
import '../models/post_model.dart';

class GetData with ChangeNotifier {
  late List<PostModel>? postModel = [];
  getData(context) async {
    postModel = await NetworkAPIService().getAPIResponse(context);

    notifyListeners();
  }
}
