import 'package:flutter/material.dart';
import 'package:lancemeupmovie/resources/color_manager.dart';

class Utlis {
  showSnakbar(String message, BuildContext context) {
    final snackbar = SnackBar(
      behavior: SnackBarBehavior.floating,
      elevation: 0,
      duration: const Duration(seconds: 4),
      content: Text(message),
      backgroundColor: ColorManager.redcolor,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackbar);
  }
}
