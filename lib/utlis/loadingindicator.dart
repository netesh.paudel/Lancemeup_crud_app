import "package:flutter/material.dart";

import '../resources/color_manager.dart';

class LoadingIndicator with ChangeNotifier {
  bool isLoading = false;
  bool isSignupLoading = false;

  Widget loading(context) {
    var platform = Theme.of(context).platform;

    return SizedBox(
      height: platform == TargetPlatform.android ? 48 : 20,
      child: CircularProgressIndicator.adaptive(
          valueColor: AlwaysStoppedAnimation<Color>(
            ColorManager.primaryBgColor,
          ),
          backgroundColor: ColorManager.primaryBgColor),
    );
  }

  updateLoading() {
    isLoading = true;
    notifyListeners();
  }

  updateNotLoading() {
    isLoading = false;
    notifyListeners();
  }

  signupLoading() {
    isSignupLoading = true;
    notifyListeners();
  }

  signupNotLoading() {
    isSignupLoading = false;
    notifyListeners();
  }
}
