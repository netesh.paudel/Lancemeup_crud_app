import 'package:flutter/material.dart';
import 'package:lancemeupmovie/utlis/routes/routes_name.dart';
import 'package:lancemeupmovie/view/authscreen/signupscreen/signupscreen.dart';
import 'package:lancemeupmovie/view/homepage/view_post/view_post.dart';
import 'package:lancemeupmovie/view/splashscreen/splashscreen.dart';

import '../../view/homepage/home_screen.dart';

class Routes {
  static Route<dynamic> generateRoutes(RouteSettings settings) {
    switch (settings.name) {
      case RouteName.home:
        return MaterialPageRoute(builder: (context) => HomeScreen());
      case RouteName.viewpost:
        var args = settings.arguments;
        return MaterialPageRoute(builder: (context) => ViewPost(args));
      case RouteName.splash:
        return MaterialPageRoute(builder: (context) => SplashScreen());
      case RouteName.signup:
        return MaterialPageRoute(builder: (context) => SignupScreen());
      default:
        return MaterialPageRoute(builder: ((context) {
          return const Scaffold(
            body: Text("No Routes"),
          );
        }));
    }
  }
}
