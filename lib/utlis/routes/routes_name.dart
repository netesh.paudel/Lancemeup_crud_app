class RouteName {
  static const String login = "login_screen";
  static const String home = "home_screen";
  static const String viewpost = "view_post";
  static const String splash = "splash_screen";
  static const String signup = "signup_screen";
}
