import 'package:flutter/material.dart';

import 'package:lancemeupmovie/view/homepage/widgets/appbar.dart';
import 'package:lancemeupmovie/view/homepage/widgets/post_tile.dart';
import 'package:lancemeupmovie/view_model/getData.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 2)).then((value) =>
        Provider.of<GetData>(context, listen: false).getData(context));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const Drawer(),
      appBar: appBar(context),
      body: Provider.of<GetData>(
                    context,
                  ).postModel ==
                  null ||
              Provider.of<GetData>(context).postModel!.isEmpty
          ? const Center(child: CircularProgressIndicator.adaptive())
          : postTile(Provider.of<GetData>(context, listen: false).postModel),
    );
  }
}
