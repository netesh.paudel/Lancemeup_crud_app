import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lancemeupmovie/resources/color_manager.dart';
import 'package:lancemeupmovie/resources/font_manager.dart';
import 'package:lancemeupmovie/resources/string_manager.dart';
import 'package:lancemeupmovie/resources/style_manager.dart';
import 'package:lancemeupmovie/respository/shared_prefrences.dart';
import 'package:lancemeupmovie/utlis/routes/routes_name.dart';

AppBar appBar(context) {
  return AppBar(
      elevation: 0,
      backgroundColor: ColorManager.bgColor,
      actions: [
        IconButton(
            onPressed: () {
              deleteUser();
              Navigator.pushNamedAndRemoveUntil(
                  context, RouteName.signup, (route) => false);
            },
            icon: Icon(
              Icons.logout,
              color: ColorManager.redcolor,
            ))
      ],
      title: Text(
        StringManager.appName,
        style: getRegularTextStyle(
            fontSize: FontSizeManager.f20,
            color: ColorManager.darkBgPrimary,
            fontWeight: FontWeightManager.semiBold),
      ));
}
