import 'package:flutter/material.dart';
import 'package:lancemeupmovie/models/post_model.dart';
import 'package:lancemeupmovie/utlis/routes/routes_name.dart';

import '../../../resources/color_manager.dart';
import '../../../resources/font_manager.dart';
import '../../../resources/string_manager.dart';
import '../../../resources/style_manager.dart';
import '../../../resources/value_manager.dart';

Padding postTile(postModel) {
  return Padding(
    padding: const EdgeInsets.all(PaddingManager.p10),
    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Expanded(
          child: ListView.separated(
        itemCount: postModel!.length,
        itemBuilder: ((context, index) {
          return Container(
            decoration: BoxDecoration(
                color: ColorManager.primaryBgColor,
                borderRadius: BorderRadius.circular(BorderRadiusManager.br16)),
            height: HeightManger.h160,
            width: MediaQuery.of(context).size.width * 0.85,
            child: Padding(
              padding: const EdgeInsets.all(PaddingManager.p20),
              child: Column(
                children: [
                  userId(postModel, index),
                  const SizedBox(
                    height: HeightManger.h10,
                  ),
                  title(postModel, index),
                  const SizedBox(
                    height: HeightManger.h16,
                  ),
                  viewPostBtn(context, index, postModel)
                ],
              ),
            ),
          );
        }),
        separatorBuilder: (BuildContext context, int index) {
          return const SizedBox(
            height: HeightManger.h14,
          );
        },
      ))
    ]),
  );
}

Row userId(_postModel, int index) {
  return Row(
    children: [
      RichText(
        text: TextSpan(children: [
          TextSpan(
              text: StringManager.id,
              style: getRegularTextStyle(
                  fontSize: FontSizeManager.f16,
                  color: ColorManager.labelTextColorBlack)),
          TextSpan(
              text: _postModel![index].id.toString(),
              style: getRegularTextStyle(
                  fontSize: FontSizeManager.f16,
                  color: ColorManager.darkFontcolor)),
        ]),
      ),
    ],
  );
}

Row title(_postModel, int index) {
  return Row(
    children: [
      Flexible(
        child: RichText(
          text: TextSpan(children: [
            TextSpan(
                text: StringManager.title,
                style: getRegularTextStyle(
                    fontSize: FontSizeManager.f16,
                    color: ColorManager.labelTextColorBlack)),
            TextSpan(
                text: _postModel![index].title,
                style: getRegularTextStyle(
                    fontSize: FontSizeManager.f16,
                    color: ColorManager.darkFontcolor)),
          ]),
        ),
      ),
    ],
  );
}

Row viewPostBtn(BuildContext context, int index, List<PostModel> postmodel) {
  return Row(
    children: [
      const Spacer(),
      Container(
        width: WidthManger.w131,
        height: HeightManger.h36,
        decoration: BoxDecoration(
            color: ColorManager.redButtoncolor,
            borderRadius: BorderRadius.circular(BorderRadiusManager.br12)),
        child: TextButton(
            onPressed: () {
              Navigator.pushNamed(context, RouteName.viewpost, arguments: {
                "index": index,
                "title": postmodel[index].title,
                "body": postmodel[index].body,
                "id": postmodel[index].id,
                "userId": postmodel[index].userId
              });
            },
            child: Text(
              StringManager.viewPost,
              style: getRegularTextStyle(
                  fontSize: FontSizeManager.f16,
                  color: ColorManager.primaryBgColor),
            )),
      )
    ],
  );
}
