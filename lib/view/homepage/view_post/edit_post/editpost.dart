import 'package:flutter/material.dart';
import 'package:lancemeupmovie/data/network/network_api_service.dart';
import 'package:lancemeupmovie/resources/color_manager.dart';
import 'package:lancemeupmovie/resources/string_manager.dart';
import 'package:lancemeupmovie/resources/value_manager.dart';
import 'package:lancemeupmovie/utlis/loadingindicator.dart';
import 'package:lancemeupmovie/utlis/snackbar.dart';

import 'package:lancemeupmovie/view/homepage/view_post/widgets/Inputfield.dart';

import 'package:provider/provider.dart';

import '../../../../resources/font_manager.dart';
import '../../../../resources/style_manager.dart';

editPost(context, args) {
  return showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: ((context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height * 0.92,
          width: MediaQuery.of(context).size.width,
          child: Padding(
            padding: const EdgeInsets.all(PaddingManager.p12),
            child: Column(
              children: [
                Text(
                  StringManager.editPost,
                  style: getRegularTextStyle(
                      fontSize: FontSizeManager.f18,
                      color: ColorManager.darkFontcolor,
                      fontWeight: FontWeightManager.semiBold),
                ),
                const SizedBox(
                  height: HeightManger.h10,
                ),
                Provider.of<InputField>(context).editInputField(args, context),
                const SizedBox(
                  height: HeightManger.h40,
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.92,
                  height: HeightManger.h60,
                  decoration: BoxDecoration(
                      color: ColorManager.redButtoncolor,
                      borderRadius:
                          BorderRadius.circular(BorderRadiusManager.br12)),
                  child: TextButton(
                      onPressed: () async {
                        Provider.of<LoadingIndicator>(context, listen: false)
                            .updateLoading();
                        await NetworkAPIService()
                            .postAPIResponse(context, args)
                            .then((value) => Navigator.pop(context))
                            .then((value) => Provider.of<LoadingIndicator>(
                                    context,
                                    listen: false)
                                .updateNotLoading());
                        Utlis().showSnakbar(StringManager.postupdated, context);
                      },
                      child:
                          Provider.of<LoadingIndicator>(context, listen: false)
                                  .isLoading
                              ? Consumer<LoadingIndicator>(
                                  builder: ((context, value, child) =>
                                      value.loading(context)))
                              : Text(
                                  StringManager.updatePost,
                                  style: getRegularTextStyle(
                                    fontSize: FontSizeManager.f18,
                                    color: ColorManager.lightFontcolor,
                                  ),
                                )),
                )
              ],
            ),
          ),
        );
      }));
}
