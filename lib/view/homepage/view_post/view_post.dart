import 'package:flutter/material.dart';

import 'package:lancemeupmovie/resources/color_manager.dart';
import 'package:lancemeupmovie/resources/font_manager.dart';
import 'package:lancemeupmovie/resources/string_manager.dart';
import 'package:lancemeupmovie/resources/style_manager.dart';
import 'package:lancemeupmovie/resources/value_manager.dart';
import 'package:lancemeupmovie/view/homepage/view_post/edit_post/editpost.dart';
import 'package:lancemeupmovie/view/homepage/view_post/widgets/appbar.dart';
import 'package:lancemeupmovie/view/homepage/view_post/widgets/delete_post.dart';

import 'package:provider/provider.dart';

import 'widgets/Inputfield.dart';

class ViewPost extends StatelessWidget {
  // ignore: prefer_typing_uninitialized_variables
  var args;
  // ignore: use_key_in_widget_constructors
  ViewPost(this.args);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: viewPostAppBar(args),
      body: Padding(
        padding: const EdgeInsets.all(PaddingManager.p12),
        child: Column(
          children: [
            Row(
              children: [
                const CircleAvatar(
                  radius: BorderRadiusManager.br26,
                  backgroundImage: AssetImage("assets/images/userimg.png"),
                ),
                const SizedBox(
                  width: WidthManger.w12,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      StringManager.username,
                      style: getRegularTextStyle(
                          fontSize: FontSizeManager.f14,
                          color: ColorManager.darkFontcolor,
                          fontWeight: FontWeightManager.semiBold),
                    ),
                    Text(
                      StringManager.email,
                      style: getRegularTextStyle(
                          fontSize: FontSizeManager.f14,
                          color: ColorManager.greyFontcolor),
                    ),
                  ],
                ),
                const Spacer(),
                IconButton(
                    onPressed: () {
                      deletesheet(context, args);
                    },
                    icon: const Icon(Icons.delete))
              ],
            ),
            const SizedBox(
              height: HeightManger.h20,
            ),
            Container(
                width: MediaQuery.of(context).size.width * 0.92,
                height: HeightManger.h220,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(
                    BorderRadiusManager.br8,
                  ),
                ),
                child: Provider.of<InputField>(
                  context,
                ).viewpostTextFormField(context, args)),
            const SizedBox(
              height: HeightManger.h40,
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.92,
              height: HeightManger.h60,
              decoration: BoxDecoration(
                  color: ColorManager.redButtoncolor,
                  borderRadius:
                      BorderRadius.circular(BorderRadiusManager.br12)),
              child: TextButton(
                  onPressed: () {
                    editPost(context, args);
                  },
                  child: Text(
                    StringManager.editPost,
                    style: getRegularTextStyle(
                      fontSize: FontSizeManager.f18,
                      color: ColorManager.lightFontcolor,
                    ),
                  )),
            )
          ],
        ),
      ),
    );
  }
}
