import 'package:flutter/material.dart';
import 'package:lancemeupmovie/resources/style_manager.dart';
import 'package:lancemeupmovie/view_model/checkIndex.dart';
import 'package:provider/provider.dart';

import '../../../../resources/color_manager.dart';
import '../../../../resources/font_manager.dart';

class InputField with ChangeNotifier {
  String? editedPost;

  Widget editInputField(args, context) {
    TextEditingController controller = TextEditingController(
        text: Provider.of<CheckIndex>(context).checkIndex(args));
    return TextFormField(
      initialValue: Provider.of<CheckIndex>(context).checkIndex(args),
      cursorColor: ColorManager.darkBgPrimary,
      style: getRegularTextStyle(
          fontSize: FontSizeManager.f16, color: ColorManager.darkFontcolor),
      maxLines: 10,
      onChanged: (value) {
        editedPost = value;
        notifyListeners();
      },
    );
  }

  Widget viewpostTextFormField(context, args) {
    TextEditingController controller = TextEditingController(
        text: Provider.of<CheckIndex>(
      context,
    ).checkIndex(args));

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      notifyListeners();
    });
    return TextFormField(
      controller: controller,
      readOnly: true,
      cursorColor: ColorManager.darkBgPrimary,
      style: getRegularTextStyle(
          fontSize: FontSizeManager.f16, color: ColorManager.darkFontcolor),
      maxLines: 10,
    );
    // ignore: dead_code
  }
}
