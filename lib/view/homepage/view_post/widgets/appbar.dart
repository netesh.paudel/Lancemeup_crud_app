import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lancemeupmovie/resources/color_manager.dart';
import 'package:lancemeupmovie/resources/font_manager.dart';
import 'package:lancemeupmovie/resources/string_manager.dart';
import 'package:lancemeupmovie/resources/style_manager.dart';

AppBar viewPostAppBar(arg) {
  return AppBar(
      elevation: 0,
      backgroundColor: ColorManager.bgColor,
      title: Text(
        StringManager.post + arg!["id"].toString(),
        style: getRegularTextStyle(
            fontSize: FontSizeManager.f20,
            color: ColorManager.darkBgPrimary,
            fontWeight: FontWeightManager.semiBold),
      ));
}
