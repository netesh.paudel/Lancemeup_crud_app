import 'package:flutter/material.dart';
import 'package:lancemeupmovie/data/network/network_api_service.dart';
import 'package:lancemeupmovie/resources/value_manager.dart';

import 'package:lancemeupmovie/view_model/editedText.dart';

import '../../../../resources/color_manager.dart';
import '../../../../resources/font_manager.dart';
import '../../../../resources/string_manager.dart';
import '../../../../resources/style_manager.dart';
import '../../../../utlis/routes/routes_name.dart';
import '../../../../utlis/snackbar.dart';

deletesheet(context, args) {
  return showModalBottomSheet(
      context: context,
      builder: ((context) {
        return Container(
          height: MediaQuery.of(context).size.height * 0.35,
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.all(PaddingManager.p10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: HeightManger.h20,
                ),
                Center(
                  child: Container(
                    height: HeightManger.h4,
                    width: WidthManger.w48,
                    decoration: BoxDecoration(
                        color: ColorManager.darkBgPrimary,
                        borderRadius:
                            BorderRadius.circular(BorderRadiusManager.br20)),
                  ),
                ),
                const SizedBox(
                  height: HeightManger.h30,
                ),
                Text(
                  StringManager.areyoursure,
                  style: getRegularTextStyle(
                      fontSize: FontSizeManager.f18,
                      color: ColorManager.darkFontcolor,
                      fontWeight: FontWeightManager.semiBold),
                ),
                const SizedBox(
                  height: HeightManger.h12,
                ),
                const SizedBox(
                  height: HeightManger.h40,
                ),
                Container(
                    height: HeightManger.h48,
                    width: MediaQuery.of(context).size.width * 0.92,
                    decoration: BoxDecoration(
                        color: ColorManager.redButtoncolor,
                        borderRadius:
                            BorderRadius.circular(BorderRadiusManager.br6)),
                    child: TextButton(
                        onPressed: () async {
                          await NetworkAPIService()
                              .deleteAPIResponse(context, args)
                              .then((value) {
                            Navigator.pushReplacementNamed(
                                context, RouteName.home);
                            Utlis().showSnakbar(
                                StringManager.postdeleted, context);
                            editedText = {};
                          });
                        },
                        child: Text(
                          StringManager.deletePost,
                          style: getRegularTextStyle(
                              fontSize: FontSizeManager.f16,
                              color: ColorManager.lightFontcolor,
                              fontWeight: FontWeightManager.semiBold),
                        ))),
              ],
            ),
          ),
        );
      }));
}
