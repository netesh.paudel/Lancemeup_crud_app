import 'package:flutter/material.dart';

import '../../../resources/value_manager.dart';

imageAnimation(_opacity, _scale) {
  return TweenAnimationBuilder(
    tween: Tween(begin: 0.0, end: 1.0),
    builder: (BuildContext context, double value, Widget? child) {
      return AnimatedOpacity(
        opacity: _opacity * value,
        duration: const Duration(seconds: 2),
        child: Transform.scale(
          scale: _scale * value,
          child: child,
        ),
      );
    },
    duration: const Duration(milliseconds: 100),
    child: const Image(
      image: AssetImage("assets/images/logo.png"),
      width: WidthManger.w178,
    ),
  );
}
