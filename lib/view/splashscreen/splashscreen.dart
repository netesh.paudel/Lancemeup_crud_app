import 'dart:async';
import 'package:flutter/material.dart';
import 'package:lancemeupmovie/respository/shared_prefrences.dart';
import 'package:lancemeupmovie/view/authscreen/signupscreen/signupscreen.dart';
import 'package:lancemeupmovie/view/homepage/home_screen.dart';

import '../../resources/color_manager.dart';
import 'animation/image_animation.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();

    Future.delayed(const Duration(milliseconds: 500)).then((value) {
      setState(() {
        _opacity = 1.0;
        _scale = 1.5;
      });
    });
    getUser();
    Timer(
        const Duration(seconds: 3),
        (() => Navigator.pushReplacement(context,
                MaterialPageRoute(builder: ((context) {
              if (token != null) {
                return HomeScreen();
              } else {
                return const SignupScreen();
              }

              // return const SignupScreen();
            })))));
  }

  double _opacity = 0.0;
  double _scale = 1.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          width: double.infinity,
          decoration: BoxDecoration(
            color: ColorManager.redcolor,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [imageAnimation(_opacity, _scale)],
          )),
    );
  }
}
