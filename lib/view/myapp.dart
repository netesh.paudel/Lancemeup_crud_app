import 'package:flutter/material.dart';
import 'package:lancemeupmovie/data/network/auth_api_services/auth_api_services.dart';
import 'package:lancemeupmovie/data/network/network_api_service.dart';
import 'package:lancemeupmovie/resources/theme_manager.dart';
import 'package:lancemeupmovie/utlis/loadingindicator.dart';
import 'package:lancemeupmovie/utlis/routes/routes.dart';
import 'package:lancemeupmovie/utlis/routes/routes_name.dart';
import 'package:lancemeupmovie/view/homepage/view_post/widgets/Inputfield.dart';
import 'package:lancemeupmovie/view_model/checkIndex.dart';
import 'package:lancemeupmovie/view_model/getData.dart';
import 'package:provider/provider.dart';

import 'authscreen/signupscreen/widgets/input_fields.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<GetData>(create: ((context) => GetData())),
        ChangeNotifierProvider<InputField>(create: ((context) => InputField())),
        ChangeNotifierProvider<NetworkAPIService>(
            create: ((context) => NetworkAPIService())),
        ChangeNotifierProvider<CheckIndex>(create: ((context) => CheckIndex())),
        ChangeNotifierProvider<LoadingIndicator>(
            create: ((context) => LoadingIndicator())),
        ChangeNotifierProvider<Textformfiled>(
            create: ((context) => Textformfiled())),
        ChangeNotifierProvider<AuthAPIServices>(
            create: ((context) => AuthAPIServices()))
      ],
      child: MaterialApp(
        theme: applicationThemeData(),
        initialRoute: RouteName.splash,
        onGenerateRoute: Routes.generateRoutes,
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
