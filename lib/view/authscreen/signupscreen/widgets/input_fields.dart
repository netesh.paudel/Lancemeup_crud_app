import 'package:flutter/material.dart';

import '../../../../resources/color_manager.dart';
import '../../../../resources/string_manager.dart';

class Textformfiled with ChangeNotifier {
  static final formkey = GlobalKey<FormState>();
  static final formkey2 = GlobalKey<FormState>();

  String email = "";
  String password = "";
  bool isObscured = true;

  //Email Field
  Widget EmailField() {
    return Form(
      key: formkey,
      child: TextFormField(
        cursorColor: ColorManager.greenThemeColor,
        textInputAction: TextInputAction.next,
        decoration: InputDecoration(
          focusColor: Colors.white,
          hintText: StringManager.hintEmailText,
        ),
        onChanged: (value) {
          email = value;
          notifyListeners();
        },
        validator: (value) {
          if (value!.isEmpty) {
            return StringManager.emailValidate;
          }
        },
      ),
    );
  }

  //Password Field
  Widget PasswordField() {
    return Form(
        key: formkey2,
        child: TextFormField(
          obscureText: isObscured,
          cursorColor: ColorManager.greenThemeColor,
          textInputAction: TextInputAction.done,
          decoration: InputDecoration(
            suffixIcon: IconButton(
              onPressed: () {
                isObscured = !isObscured;
                notifyListeners();
              },
              icon: Icon(isObscured
                  ? Icons.visibility_outlined
                  : Icons.visibility_off_rounded),
            ),
            focusColor: Colors.white,
            hintText: StringManager.hintPasswordText,
          ),
          onChanged: (value) {
            password = value;
            notifyListeners();
          },
          validator: ((value) {
            if (value!.isEmpty) {
              return StringManager.passwwordValidate;
            }
          }),
        ));
  }
}
