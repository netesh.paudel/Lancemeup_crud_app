import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../../resources/color_manager.dart';
import '../../../../resources/string_manager.dart';

AppBar signupappBar() {
  return AppBar(
    systemOverlayStyle: SystemUiOverlayStyle(
        statusBarColor: ColorManager.primaryBgColor,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.light),
    elevation: 0,
    centerTitle: false,
    backgroundColor: ColorManager.redcolor,
    title: Text(
      StringManager.signup,
    ),
  );
}
