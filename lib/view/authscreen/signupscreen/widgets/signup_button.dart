import 'package:flutter/material.dart';
import 'package:lancemeupmovie/data/network/auth_api_services/auth_api_services.dart';
import 'package:lancemeupmovie/utlis/routes/routes_name.dart';

import 'package:provider/provider.dart';
import '../../../../resources/color_manager.dart';
import '../../../../resources/font_manager.dart';
import '../../../../resources/string_manager.dart';
import '../../../../resources/style_manager.dart';
import '../../../../resources/value_manager.dart';
import '../../../../utlis/loadingindicator.dart';

import 'input_fields.dart';

Widget SignupButton(BuildContext context) {
  return Container(
    width: WidthManger.w358,
    height: HeightManger.h48,
    decoration: BoxDecoration(
        color: ColorManager.redcolor, borderRadius: BorderRadius.circular(6)),
    child: TextButton(
        onPressed: () async {
          if (Textformfiled.formkey.currentState!.validate() &
              Textformfiled.formkey2.currentState!.validate()) {
            Provider.of<LoadingIndicator>(context, listen: false)
                .signupLoading();
            await AuthAPIServices().authAPIResponse(context).then((value) =>
                Navigator.pushReplacementNamed(context, RouteName.home));

            Provider.of<LoadingIndicator>(context, listen: false)
                .signupNotLoading();
          }
        },
        child: Consumer<LoadingIndicator>(
          builder: ((context, value, child) => value.isSignupLoading
              ? value.loading(context)
              : Text(
                  StringManager.signup,
                  style: getRegularTextStyle(
                      fontSize: FontSizeManager.f16,
                      color: ColorManager.lightFontcolor,
                      fontWeight: FontWeightManager.semiBold),
                )),
        )),
  );
}
