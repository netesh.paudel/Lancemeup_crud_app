import 'package:flutter/material.dart';
import 'package:lancemeupmovie/view/authscreen/signupscreen/widgets/appbar.dart';
import 'package:lancemeupmovie/view/authscreen/signupscreen/widgets/input_fields.dart';
import 'package:lancemeupmovie/view/authscreen/signupscreen/widgets/signup_button.dart';

import 'package:provider/provider.dart';

import '../../../resources/color_manager.dart';
import '../../../resources/font_manager.dart';
import '../../../resources/string_manager.dart';
import '../../../resources/style_manager.dart';
import '../../../resources/value_manager.dart';

class SignupScreen extends StatelessWidget {
  const SignupScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: signupappBar(), body: SignupBodyUI(context));
  }

  SignupBodyUI(BuildContext context) {
    return SingleChildScrollView(
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.85,
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(
                left: PaddingManager.p16, right: PaddingManager.p16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: HeightManger.h20,
                ),
                Text(
                  StringManager.appName,
                  style: getRegularTextStyle(
                      fontSize: FontSizeManager.f20,
                      color: ColorManager.darkFontcolor,
                      fontWeight: FontWeightManager.semiBold),
                ),
                const SizedBox(
                  height: HeightManger.h10,
                ),

                const SizedBox(
                  height: HeightManger.h30,
                ),

                //Email Input field
                Text(
                  StringManager.Email,
                  style: getRegularTextStyle(
                      fontSize: FontSizeManager.f16,
                      color: ColorManager.darkFontcolor),
                ),
                const SizedBox(
                  height: HeightManger.h10,
                ),
                Provider.of<Textformfiled>(
                  context,
                ).EmailField(),
                const SizedBox(
                  height: HeightManger.h30,
                ),

                //Password Form Field
                Text(
                  StringManager.password,
                  style: getRegularTextStyle(
                      fontSize: FontSizeManager.f16,
                      fontWeight: FontWeightManager.regular,
                      color: ColorManager.darkFontcolor),
                ),
                const SizedBox(
                  height: HeightManger.h10,
                ),
                Provider.of<Textformfiled>(
                  context,
                ).PasswordField(),
                const SizedBox(
                  height: HeightManger.h100,
                ),

                //Login Button
                SignupButton(context),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
