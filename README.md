# Lancemeup CRUD APP

This app is created in flutter using provider .

## Getting Started

This application is to show CRUD operation using RestAPI .

For CRUD operation, I have use JSONPlaceholder . To get more info visit following link :
```
https://jsonplaceholder.typicode.com/
```
For authencation, I have used reqres. To get more info visit following link : 
```
https://reqres.in/
```

## Demo 

![Simulator Screen Recording - iPhone 14 Pro - 2023-03-06 at 23 10 35](https://user-images.githubusercontent.com/48326144/223185287-c398ad6a-a4b0-4aca-907f-88f816f4313a.gif)

## Some Screenshots



<img width="250" height="550" src="https://user-images.githubusercontent.com/48326144/223185644-1a235b4f-ea34-4b0c-a4a6-5c930b08e792.png">   <img width="250" height="550" src="https://user-images.githubusercontent.com/48326144/223185653-9ba2e3e2-7d84-46c2-9156-000c2ea01c62.png">     <img width="250" height="550" src="https://user-images.githubusercontent.com/48326144/223185659-41b43c39-0ecb-4d9c-b5c4-62dd69b29b66.png">

<img width="250" height="550" src="https://user-images.githubusercontent.com/48326144/223185661-bc81ca48-e052-40f4-8b90-ea5827866c63.png">   <img width="250" height="550" src="https://user-images.githubusercontent.com/48326144/223185666-8aa565b3-a951-43ea-a08e-b6a3ff1d5619.png">   <img width="250" height="550" src="https://user-images.githubusercontent.com/48326144/223185670-bb083ccb-2198-4051-8342-e51d93a3e445.png">

<img width="250" height="550" src="https://user-images.githubusercontent.com/48326144/223185674-15a71992-f966-40a0-acfd-26be15b88a0e.png">

## To login use this credential
**Email**
```
eve.holt@reqres.in
```
**Password**
```
pistol
```



## How to Use 

**Step 1:**

Download or clone this repo by using the link below:
```
https://github.com/Netesh5/Lancemeup_crud_app
```

**Step 2:**

Go to project root and execute the following command in console to get the required dependencies: 

```
flutter pub get 
```

**OR Downlaod the apk from link below**
```
https://drive.google.com/file/d/1vmpbIg8XynMZ-RWdAc7LtEF6RVuCNiX_/view?usp=share_link
```


## App Features:

* Splash
* Login
* Home
* Routing
* Theme
* Provider (State Management)

### Folder Structure
Here is the core folder structure which flutter provides.

```
flutter-app/
|- android
|- build
|- ios
|- lib
|- test
```





